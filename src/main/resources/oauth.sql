CREATE TABLE IF NOT EXISTS users
(
   id        varchar(255)   NOT NULL,
   password  varchar(255),
   username  varchar(255)
);

ALTER TABLE users ADD CONSTRAINT users_pkey PRIMARY KEY (id);

CREATE TABLE IF NOT EXISTS user_roles
(
    id         bigint         NOT NULL,
    role_name  varchar(255)
);

ALTER TABLE user_roles ADD CONSTRAINT user_roles_pkey PRIMARY KEY (id);

CREATE TABLE IF NOT EXISTS users_roles
(
    user_id   varchar(255)   NOT NULL,
    roles_id  bigint         NOT NULL
);

ALTER TABLE users_roles
   ADD CONSTRAINT uniqueval UNIQUE (roles_id);

ALTER TABLE users_roles
  ADD CONSTRAINT FK1 FOREIGN KEY (roles_id)
  REFERENCES user_roles (id)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION;

ALTER TABLE users_roles
  ADD CONSTRAINT FK2 FOREIGN KEY (user_id)
  REFERENCES users (id)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION;