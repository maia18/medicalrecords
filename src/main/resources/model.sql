CREATE SEQUENCE annotation_seq
       INCREMENT BY 1
       MINVALUE 1
       CACHE 1
       NO CYCLE;

CREATE SEQUENCE imagetest_seq
       INCREMENT BY 1
       MINVALUE 1
       CACHE 1
       NO CYCLE;

CREATE SEQUENCE patient_seq
       INCREMENT BY 1
       MINVALUE 1
       CACHE 1
       NO CYCLE;

CREATE SEQUENCE record_seq
       INCREMENT BY 1
       MINVALUE 1
       CACHE 1
       NO CYCLE;

CREATE TABLE IF NOT EXISTS doctors_has_imagetest
(
   doctors_doctorid   integer    NOT NULL,
   imagetest_testid   integer    NOT NULL,
   anotationdate      date           NOT NULL,
   anotationcomments  text   NOT NULL,
   id                 integer        NOT NULL
);

ALTER TABLE doctors_has_imagetest
   ADD CONSTRAINT doctors_has_imagetest_pkey
   PRIMARY KEY (id);

CREATE TABLE IF NOT EXISTS imagetest
(
   testid                         integer    NOT NULL,
   imagedate                      date,
   imagetype                      varchar(256)    NOT NULL,
   manufacturerimg                varchar(256),
   modalityimg                    varchar(256),
   studydescription               varchar(256),
   seriesdescription              varchar(256),
   imageroute                     varchar(250),
   medicalrecord_medicalrecordid  integer    NOT NULL
);

ALTER TABLE imagetest
   ADD CONSTRAINT imagetest_pkey
   PRIMARY KEY (testid);

CREATE TABLE IF NOT EXISTS medicalrecord
(
   medicalrecordid       integer    NOT NULL,
   reportdate            date,
   studyname             varchar(256)    NOT NULL,
   diagnosisdescription  text,
   diagnosiscomments     text,
   diagnosticcol         text,
   patientid             integer        NOT NULL,
   userid                varchar(256)   NOT NULL
);

ALTER TABLE medicalrecord
   ADD CONSTRAINT medicalrecord_pkey
   PRIMARY KEY (medicalrecordid);

CREATE TABLE IF NOT EXISTS patients
(
   patientid          integer        NOT NULL,
   idtypepatient      varchar(256),
   dnipatient         varchar(256)    NOT NULL,
   lastnamepatient    varchar(256),
   namepatient        varchar(256),
   genderpatient      varchar(1),
   birthdatepatient   date,
   birthplacepatient  varchar(256),
   addresspatient     varchar(256),
   phonepatient       varchar(256),
   emailpatient       varchar(256),
   statuspatient      varchar(256),
   childrenpatient    varchar(256)
);

ALTER TABLE patients
   ADD CONSTRAINT patients_pkey
   PRIMARY KEY (patientid);

CREATE TABLE IF NOT EXISTS role
(
   idrole    integer       NOT NULL,
   rolename  varchar(256)
);

ALTER TABLE role
   ADD CONSTRAINT role_pkey
   PRIMARY KEY (idrole);

CREATE TABLE IF NOT EXISTS systemusers
(
   userid          varchar(256)   NOT NULL,
   passworduser    varchar(256),
   idtypeuser      varchar(256),
   dniuser         varchar(256)   NOT NULL,
   lastnameuser    varchar(256),
   nameuser        varchar(256),
   genderuser      varchar(1),
   birthdateuser   date,
   birthplaceuser  varchar(256),
   addressuser     varchar(256),
   phoneuser       varchar(256),
   departmentuser  varchar(256),
   emailuser       varchar(256),
   role_idrole     integer        NOT NULL
);

ALTER TABLE systemusers
   ADD CONSTRAINT systemusers_pkey
   PRIMARY KEY (userid);




ALTER TABLE doctors_has_imagetest
  ADD CONSTRAINT fk_doctors_has_imagetest_imagetest1 FOREIGN KEY (imagetest_testid)
  REFERENCES imagetest (testid)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION;

ALTER TABLE medicalrecord
  ADD CONSTRAINT fk_medicalrecord_patients1 FOREIGN KEY (patientid)
  REFERENCES patients (patientid)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION;

ALTER TABLE medicalrecord
  ADD CONSTRAINT fk_medicalrecord_systemusers FOREIGN KEY (userid)
  REFERENCES systemusers (userid)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION;

ALTER TABLE systemusers
  ADD CONSTRAINT fk_user_role FOREIGN KEY (role_idrole)
  REFERENCES role (idrole)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION;


INSERT INTO role (idrole,rolename)
VALUES
  (1,'ROLE_ADMIN'),
  (2,'ROLE_RMAN'),
  (3,'ROLE_PROF'),
  (4,'ROLE_VIEWER'),
  (5,'ROLE_FEEDER');

INSERT INTO systemusers (userid,passworduser,idtypeuser,dniuser,lastnameuser,nameuser,genderuser,birthdateuser,birthplaceuser,addressuser,phoneuser,departmentuser,emailuser,role_idrole)
VALUES
  ('admin','$2a$10$OvaPRgSLkBpEfJ6Tmrg4L.pE1ICTX5PVH7tV0ZDREkizBRoqDKlSi','PASSPORT','AO432123','Gomez','Jhon M','M',DATE '2019-05-05','COLOMBIA','Calle Falsa 123','12341231121','MRI','me@gmail.com',1);


COMMIT;
