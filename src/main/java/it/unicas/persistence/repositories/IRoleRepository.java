package it.unicas.persistence.repositories;

import it.unicas.persistence.entities.Role;
import org.springframework.data.repository.CrudRepository;

public interface IRoleRepository extends CrudRepository<Role, Long>
{
    Role findByRolename(String rolename);
}
