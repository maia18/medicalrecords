package it.unicas.persistence.repositories;

import it.unicas.persistence.entities.MedicalRecord;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;


@RepositoryRestResource(exported = false)
public interface IMedicalRecordsRepository extends CrudRepository<MedicalRecord, Long>
{
}
