package it.unicas.persistence.repositories;

import it.unicas.persistence.entities.DocumentType;
import it.unicas.persistence.entities.Patient;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(exported = false)
public interface IPatientsRepository extends CrudRepository<Patient, Long>
{
    List<Patient> findAllByDocIdAndDocType(String docID, DocumentType documentType);
    Patient findByDocIdAndDocType(String docID, DocumentType documentType);

    List<Patient> findAllByDocIdLikeAndNameLikeAndLastnameLike(
            String document, String name, String lastname);
}

//SELECT * FROM Patients WHERE DOC_TYPE = {doctype} AND DOC_NUMBER = {DOC_number}: