package it.unicas.persistence.repositories;

import it.unicas.persistence.entities.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(exported = false)
public interface IUserRepository extends CrudRepository<User, String>
{
    @Query(value = "SELECT u from Systemusers u WHERE u.userid LIKE :userid AND u.dniuser LIKE :document"
            + " AND u.nameuser LIKE :nameuser AND u.lastnameuser LIKE :lastname u.role_idrole <> (SELECT idrole FROM role WHERE rolename = 'ROLE_ADMIN')"
            +   " AND u.role_idrole <> (SELECT idrole FROM role WHERE rolename = 'ROLE_RMAN')", nativeQuery = true)
    List<User> findByRoleNoAdmin(
        @Param("userid") String userid,
        @Param("document") String document,
        @Param("nameuser") String name,
        @Param("lastname") String lastname);

    List<User> findAllByIdLikeAndNameLikeAndLastNameLikeAndDocumentNumberLike(
        String id, String name, String lastname, String document
    );
}
