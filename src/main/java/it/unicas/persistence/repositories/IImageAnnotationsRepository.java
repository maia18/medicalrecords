package it.unicas.persistence.repositories;

import org.springframework.data.repository.CrudRepository;
import it.unicas.persistence.entities.ImageAnnotations;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;


@RepositoryRestResource(exported = false)
public interface IImageAnnotationsRepository extends CrudRepository<ImageAnnotations, Long>
{
}
