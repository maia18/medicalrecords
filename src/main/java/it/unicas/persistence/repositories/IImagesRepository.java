package it.unicas.persistence.repositories;

import it.unicas.persistence.entities.Images;
import it.unicas.persistence.entities.MedicalRecord;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;


@RepositoryRestResource(exported = false)
public interface IImagesRepository extends CrudRepository<Images, Long>
{
    List<Images> findByRecordid(long recordid);
}
