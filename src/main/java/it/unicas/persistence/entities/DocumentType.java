package it.unicas.persistence.entities;

public enum DocumentType
{
    PASSPORT,
    NATIONAL_ID,
    RESIDENCE_PERMIT_NUMBER,
    OTHER
}
