package it.unicas.persistence.entities;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "medicalrecord")
@Data
public class MedicalRecord //implements Serializable
{

    @Column(name = "medicalrecordid")
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "id_record")
    @SequenceGenerator(name="id_record", sequenceName = "record_seq",  allocationSize = 1)
    private long id;

    @Column(name = "reportdate")
    private Date reportdate;

    @Column(name = "studyname")
    private String studyname;

    @Column(name = "diagnosisdescription")
    private String diagnosisdescription;

    @Column(name = "diagnosiscomments")
    private String diagnosiscomments;

    @Column(name = "patientid")
    private long patientId;

    @Column(name = "userid")
    private String userid;

}
