package it.unicas.persistence.entities;


import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "imagetest")
@Data
public class Images
{

    @Column(name = "testid")
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "id_image")
    @SequenceGenerator(name="id_image", sequenceName = "imagetest_seq", allocationSize = 1)
    private long id;

    @Column(name = "imagedate")
    private Date strudyDate;

    @Column(name ="imagetype")
    private String type;

    @Column(name = "manufacturerimg")
    private String manufacturer;

    @Column(name = "modalityimg")
    private String modality;

    @Column(name = "studydescription")
    private String description;

    @Column(name = "seriesdescription")
    private String series;

    @Column(name = "imageroute")
    private String path;

    @Column(name = "medicalrecord_medicalrecordid")
    private long recordid;
}
