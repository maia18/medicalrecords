package it.unicas.persistence.entities;

import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.EnumType;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;

@Entity
@Table(name = "systemusers")
@Data
public class User
{
    @Id
    @Column(name = "userid", nullable = false)
    private String id;

    @Enumerated(EnumType.STRING)
    @Column(name = "idtypeuser", nullable = false)
    private DocumentType documentType;

    @Column(name = "passworduser")
    private String encryptedPassword;

    @Column(name = "dniuser", nullable = false)
    private String documentNumber;

    @Column(name = "lastnameuser")
    private String lastName;

    @Column(name = "nameuser")
    private String name;

    @Enumerated(EnumType.STRING)
    @Column(name = "genderuser")
    private GenderType gender;

    @Column(name = "birthdateuser")
    private Date birthDate;

    @Column(name = "birthplaceuser")
    private String birthPlace;

    @Column(name = "addressuser")
    private String address;

    @Column(name = "phoneuser")
    private String phone;

    @Column(name = "departmentuser")
    private String department;

    @Column(name = "emailuser")
    private String email;

    @ManyToOne(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
    @JoinColumn(name = "role_idrole", referencedColumnName = "idrole")
    private Role role;
}
