package it.unicas.persistence.entities;

public enum GenderType
{
    M,
    F,
    O
}
