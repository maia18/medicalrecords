package it.unicas.persistence.entities;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "doctors_has_imagetest")
@Data
public class ImageAnnotations //implements Serializable
{

    @Column(name = "id")
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "id_anno")
    @SequenceGenerator(name="id_anno", sequenceName = "record_seq",  allocationSize = 1)
    private long id;

    @Column(name = "imagetest_testid")
    private String imageId;

    @Column(name = "doctors_doctorid")
    private String doctorId;


    @ManyToOne(fetch = FetchType.EAGER, cascade=CascadeType.ALL, targetEntity = User.class)
    @JoinColumn(name = "doctors_doctorid", referencedColumnName = "userid",  updatable = false,insertable = false)
    private User doctor;

    @Column(name = "anotationdate")
    private Date annotationDate;

    @Column(name = "anotationcomments")
    private String annotationComments;

}
