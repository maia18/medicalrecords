package it.unicas.persistence.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Entity
@Table(name = "role")
@Data
public class Role
{
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "idrole")
    private int id;

    @Column
    private String rolename;

    public enum Roles
    {
        ROLE_ADMIN, ROLE_RMAN, ROLE_VIEWER, ROLE_PROF, ROLE_FEEDER
    }
}
