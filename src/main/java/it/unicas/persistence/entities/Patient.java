package it.unicas.persistence.entities;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "patients")
@Data
public class Patient
{
    @Column(name = "patientid")
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "id_patient")
    @SequenceGenerator(name="id_patient", sequenceName = "patient_seq",  allocationSize = 1)
    private long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "idtypepatient")
    private DocumentType docType;

    @Column(name = "dnipatient")
    private String docId;

    @Column(name = "lastnamepatient")
    private String lastname;

    @Column(name = "namepatient")
    private String name;

    @Enumerated(EnumType.STRING)
    @Column(name = "genderpatient")
    private GenderType gender;

    @Column(name = "birthdatepatient")
    private Date birthdate;

    @Column(name = "birthplacepatient")
    private String birthplace;

    @Column(name = "addresspatient")
    private String address;

    @Column(name = "phonepatient")
    private String phone;

    @Column(name = "emailpatient")
    private String email;

    @OneToMany(fetch = FetchType.EAGER, cascade=CascadeType.ALL, targetEntity = MedicalRecord.class)
    @JoinColumn(name = "patientid", referencedColumnName = "patientid", updatable = false,insertable = false)
    List<MedicalRecord> medicalRecordList;

    @Column(name = "statuspatient")
    private String status;

    @Column(name = "childrenpatient")
    private String child;


}
