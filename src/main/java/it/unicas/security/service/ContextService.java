package it.unicas.security.service;

import java.util.List;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * Helper class for accessing the data contained in the SecurityContext
 */
public class ContextService
{
    /**
     * Retrieves the authenticated user from the SecurityContextHolder.
     *
     * @return a UserInformation object.
     */
    public static UserInformation loadUserInformation()
    {
        List<GrantedAuthority> roles =
            (List<GrantedAuthority>)
                SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        String role = roles.get(0).getAuthority();
        String username =
            SecurityContextHolder.getContext().getAuthentication().getName();

        UserInformation information = new UserInformation(username, role);
        return  information;
    }
}
