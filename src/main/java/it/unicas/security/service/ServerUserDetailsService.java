package it.unicas.security.service;

import it.unicas.persistence.entities.User;
import it.unicas.persistence.repositories.IUserRepository;
import it.unicas.security.resources.ServerUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ServerUserDetailsService implements UserDetailsService
{

    @Autowired
    private IUserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException
    {
        Optional<User> user = userRepository.findById(userName);
        if(user.isEmpty())
        {
            throw new UsernameNotFoundException("User " + userName + " not found");
        }
        return new ServerUserDetails(user.get());
    }

}
