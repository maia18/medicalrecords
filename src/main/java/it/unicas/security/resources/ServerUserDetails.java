package it.unicas.security.resources;

import it.unicas.persistence.entities.Role;
import it.unicas.persistence.entities.User;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class ServerUserDetails implements UserDetails
{

    private static final long serialVersionUID = 1L;
    private Collection<? extends GrantedAuthority> authorities;
    private String password;
    private String username;

    public ServerUserDetails(User user)
    {
        this.username = user.getId();
        this.password = user.getEncryptedPassword();
        this.authorities = translate(user.getRole());
    }

    private Collection<? extends GrantedAuthority> translate(Role role)
    {
        List<GrantedAuthority> authorities = new ArrayList<>();

        String name = role.getRolename().toUpperCase();
        if (!name.startsWith("ROLE_"))
        {
            name = "ROLE_" + name;
        }
        authorities.add(new SimpleGrantedAuthority(name));
        return authorities;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities()
    {
        return authorities;
    }

    @Override
    public String getPassword()
    {
        return password;
    }

    @Override
    public String getUsername()
    {
        return username;
    }

    @Override
    public boolean isAccountNonExpired()
    {
        return true;
    }

    @Override
    public boolean isAccountNonLocked()
    {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired()
    {
        return true;
    }

    @Override
    public boolean isEnabled()
    {
        return true;
    }

}
