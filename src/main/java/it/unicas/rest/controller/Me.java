package it.unicas.rest.controller;

import it.unicas.rest.controller.users.UserDTO;
import it.unicas.rest.controller.users.UsersService;
import it.unicas.security.service.ContextService;
import it.unicas.security.service.UserInformation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/me")
public class Me
{
    private UsersService usersService;

    @Autowired
    Me(
        final UsersService usersService)
    {
        this.usersService = usersService;
    }

    @GetMapping
    public UserInformation loadUserInfo()
    {
        return ContextService.loadUserInformation();
    }

    @GetMapping("/allinfo")
    public ResponseEntity<UserDTO> getMyInfo()
    {
        UserInformation info = ContextService.loadUserInformation();
        String userName = info.getUsername();

        ResponseEntity<UserDTO> response = null;
        try
        {
            UserDTO user = usersService.retrieveUserById(userName);
            response = ResponseEntity.ok(user);
        }
        catch (ResourceNotFoundException e)
        {
            response = ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
        return response;
    }

    @PutMapping("/changepass")
    public ResponseEntity<Void> changePassword(@RequestBody UserDTO dto)
    {
        UserInformation info = ContextService.loadUserInformation();
        String username = info.getUsername();

        try
        {
            usersService.changePassword(username, dto.getPassword());
            return ResponseEntity.ok().build();
        }
        catch (ResourceNotFoundException e)
        {
            return ResponseEntity.notFound().build();
        }
    }
}
