package it.unicas.rest.controller.medicalrecords;

import it.unicas.persistence.entities.MedicalRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/medrec")

@Secured({"ROLE_ADMIN", "ROLE_PROF"})
public class MedicalRecordController
{
    private final MedicalRecordService service;

    @Autowired
    MedicalRecordController(
            MedicalRecordService service
    )
    {
        this.service = service;
    }

    @Secured({"ROLE_ADMIN", "ROLE_PROF"})
    @RequestMapping(path = "/{patientId}", method = RequestMethod.PUT)
    public ResponseEntity<Void> create(@RequestBody MedicalRecordDTO medicalRecordDTO, @PathVariable String patientId)
    {
        ResponseEntity<Void> response = null;
        try {
            service.createmedicalRecord(medicalRecordDTO, Long.valueOf(patientId));
            response = ResponseEntity.ok().build();
        }
        catch (ResourceNotFoundException e)
        {
            response = ResponseEntity.notFound().build();
        }
        catch (UnsupportedOperationException e)
        {
            response = ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        return response;
    }


}
