package it.unicas.rest.controller.medicalrecords;

import it.unicas.rest.controller.users.UserDTO;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class MedicalRecordDTO {
    private String Id;
    private String reportDate;
    private String studyName;
    private String diagnosisDescription;
    private String diagnosisComments;
    private String patientId;
    private UserDTO user;
    private List<Integer> images;
}
