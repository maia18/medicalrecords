package it.unicas.rest.controller.medicalrecords;

import it.unicas.persistence.entities.MedicalRecord;
import it.unicas.persistence.repositories.IMedicalRecordsRepository;
import it.unicas.rest.controller.users.UserDTO;
import it.unicas.security.service.ContextService;
import it.unicas.security.service.ServerUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

@Component
public class MedicalRecordService {

    private final MedicalRecordMapper medicalRecordMapper;
    private final IMedicalRecordsRepository medicalRecordsRepository;

    @Autowired
    MedicalRecordService(
            final MedicalRecordMapper medicalRecordMapper,
            final IMedicalRecordsRepository medicalRecordsRepository
    )
    {
        this.medicalRecordMapper = medicalRecordMapper;
        this.medicalRecordsRepository = medicalRecordsRepository;
    }

    @Transactional(rollbackFor = Exception.class)
    public String createmedicalRecord(final MedicalRecordDTO medicalRecordDTO, Long userId)
    {
        UserDTO userDTO = new UserDTO();
        userDTO.setUserId(ContextService.loadUserInformation().getUsername());
        medicalRecordDTO.setUser(userDTO);
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        medicalRecordDTO.setReportDate(format.format(new Date()));
        MedicalRecord record = medicalRecordMapper.newMedicalRecordBean(medicalRecordDTO, userId);
        MedicalRecord newRecord = medicalRecordsRepository.save(record);
        String newRecordIdStr = String.valueOf(newRecord.getId());
        return newRecordIdStr;
    }

    public MedicalRecordDTO getMedicalRecord(String medicalRecordId)
    {
        Optional<MedicalRecord> medicalRecord = medicalRecordsRepository.findById(Long.parseLong(medicalRecordId));
        if (medicalRecord.isPresent())
            return medicalRecordMapper.newMedicalRecordDTO(medicalRecord.get());
        else
            return null;
    }
}
