package it.unicas.rest.controller.medicalrecords;

import it.unicas.persistence.entities.MedicalRecord;
import it.unicas.rest.controller.images.ImagesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import it.unicas.rest.controller.users.UserMapper;
import it.unicas.rest.controller.users.UsersService;

import java.text.SimpleDateFormat;
import java.text.ParseException;


@Component
public class MedicalRecordMapper
{
    UsersService usersService;
    ImagesService imagesService;

    @Autowired
    private MedicalRecordMapper(UserMapper usermapper, UsersService usersService, ImagesService imagesService)
    {
        this.usersService = usersService;
        this.imagesService = imagesService;
    }

    public MedicalRecord newMedicalRecordBean(MedicalRecordDTO medicalRecordDTO, Long patientId)
    {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        MedicalRecord record = new MedicalRecord();
        record.setStudyname(medicalRecordDTO.getStudyName());
        record.setDiagnosisdescription(medicalRecordDTO.getDiagnosisDescription());
        record.setDiagnosiscomments(medicalRecordDTO.getDiagnosisComments());
        record.setUserid(medicalRecordDTO.getUser().getUserId());
        record.setPatientId(patientId);
        try
        {
            record.setReportdate(formatter.parse(medicalRecordDTO.getReportDate()));
        }
        catch (ParseException exception)
        {
            throw new RuntimeException(exception.getMessage());
        }
        return record;
    }

    public MedicalRecordDTO newMedicalRecordDTO(MedicalRecord medicalRecord)
    {
        MedicalRecordDTO dto = new MedicalRecordDTO();
        dto.setId(String.valueOf(medicalRecord.getId()));
        dto.setReportDate(medicalRecord.getReportdate().toString());
        dto.setStudyName(medicalRecord.getStudyname());
        dto.setDiagnosisComments(medicalRecord.getDiagnosiscomments());
        dto.setDiagnosisDescription(medicalRecord.getDiagnosisdescription());
        dto.setUser(usersService.retrieveUserById(medicalRecord.getUserid()));
        dto.setImages(imagesService.getImagesIdsByMedRec("" + medicalRecord.getId()));
        dto.setPatientId(String.valueOf(medicalRecord.getPatientId()));
        return dto;
    }
}
