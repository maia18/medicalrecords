package it.unicas.rest.controller.common;

public class ResourceAlreadyExistsException extends RuntimeException
{
    public ResourceAlreadyExistsException(String message)
    {
        super(message);
    }
}
