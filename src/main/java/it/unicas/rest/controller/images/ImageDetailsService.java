package it.unicas.rest.controller.images;

import it.unicas.rest.controller.patients.PatientDTO;
import it.unicas.rest.controller.patients.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ImageDetailsService
{
    private final ImagesService imagesService;
    private final PatientService patientService;

    @Autowired ImageDetailsService(ImagesService imagesService, PatientService patientService)
    {
        this.imagesService = imagesService;
        this.patientService = patientService;
    }
    public ImageDetailsDTO getDetailsPerImage(String id)
    {
        ImagesDTO imageDTO = imagesService.getImageById(id);
        ImageDetailsDTO imageDetailsDTO = new ImageDetailsDTO();

        if (imageDTO == null)
            return null;

        // Retrieve patient info.
        PatientDTO patientDTO = patientService.getPatientInformationFromMedicalRecord(imageDTO.getRecordId());

        imageDetailsDTO.setPatient(patientDTO);
        imageDetailsDTO.setImage(imageDTO);
        return imageDetailsDTO;
    }
}
