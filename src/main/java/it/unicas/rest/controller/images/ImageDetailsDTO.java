package it.unicas.rest.controller.images;

import it.unicas.rest.controller.patients.PatientDTO;
import lombok.Data;

@Data
public class ImageDetailsDTO
{
    PatientDTO patient;
    ImagesDTO image;
}
