package it.unicas.rest.controller.images;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class ImagesDTO
{
    private MultipartFile file;
    private long id;
    private String strudyDate;
    private String type;
    private String manufacturer;
    private String modality;
    private String description;
    private String series;
    private String path;
    private String recordId;
}
