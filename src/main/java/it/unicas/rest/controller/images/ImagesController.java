package it.unicas.rest.controller.images;

import it.unicas.rest.controller.medicalrecords.MedicalRecordDTO;
import it.unicas.rest.controller.medicalrecords.MedicalRecordService;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;

@RestController
@RequestMapping("/api/images")
@Secured({"ROLE_ADMIN", "ROLE_PROF"})
public class ImagesController
{
    ImagesService service;
    MedicalRecordService medRecService;
    ImageDetailsService imageDetailsService;

    @Autowired
    ImagesController(ImagesService service, ImageDetailsService imageDetailsService, MedicalRecordService medicalRecordService)
    {
        this.service = service;
        medRecService = medicalRecordService;
        this.imageDetailsService = imageDetailsService;
    }

    @Secured({"ROLE_ADMIN", "ROLE_FEEDER"})
    @RequestMapping(path = "/{medrecId}", method = RequestMethod.PUT)
    ResponseEntity<Void> loadImageInRepo(
        @RequestParam MultipartFile file,
        @RequestParam String manufacturer,
        @RequestParam String studyDate,
        @RequestParam String modality,
        @RequestParam String description,
        @RequestParam String series,
        @PathVariable String medrecId) throws URISyntaxException
    {
        ImagesDTO imagesDTO = new ImagesDTO();
        imagesDTO.setManufacturer(manufacturer);
        imagesDTO.setStrudyDate(studyDate);
        imagesDTO.setModality(modality);
        imagesDTO.setDescription(description);
        imagesDTO.setSeries(series);
        imagesDTO.setFile(file);

        ResponseEntity<Void> response = null;

        MedicalRecordDTO medrecdto = medRecService.getMedicalRecord(medrecId);
        if (medrecdto == null)
        {
            response = ResponseEntity.notFound().build();
        }
        else
        {
            String filename = this.service.readAndSaveFile(imagesDTO.getFile(), medrecId);
            String imageId = service.createImage(imagesDTO, medrecId, filename);
            response = ResponseEntity.created(new URI("api/images/" + medrecId + "/" + imageId)).build();
        }
        return response;
    }


    @Secured({"ROLE_ADMIN", "ROLE_PROF", "ROLE_VIEWER"})
    @GetMapping(
        path = {"/fullsize"},
        params = {"img"},
        produces = MediaType.IMAGE_JPEG_VALUE
    )
    public ResponseEntity<byte[]> getImage(
        @RequestParam("img") String imageId) throws IOException
    {
        ImagesDTO dto = service.getImageById(imageId);
        if (dto == null)
        {
            return ResponseEntity.notFound().build();
        }

        String filepath = service.getFilesDir() + dto.getPath();
        File file = new File(filepath);
        System.out.println(file.getAbsolutePath());
        InputStream in = new FileInputStream(file);
        return ResponseEntity.ok(IOUtils.toByteArray(in));
    }

    @Secured({"ROLE_ADMIN", "ROLE_PROF", "ROLE_VIEWER"})
    @GetMapping(
        path = {"/thumb"},
        params = {"img"},
        produces = MediaType.IMAGE_JPEG_VALUE
    )
    public ResponseEntity<byte[]> getImageThumb(
            @RequestParam("img") String imageId) throws IOException
    {
        ImagesDTO dto = service.getImageById(imageId);
        if (dto == null)
        {
            return ResponseEntity.notFound().build();
        }

        String filepath = service.getThumbsDir() + dto.getPath();
        File file = new File(filepath);
        System.out.println(file.getAbsolutePath());
        InputStream in = new FileInputStream(file);
        return ResponseEntity.ok(IOUtils.toByteArray(in));
    }

    @Secured({"ROLE_ADMIN", "ROLE_PROF", "ROLE_VIEWER"})
    @GetMapping(path ="/{imageid}")
    public ResponseEntity<ImageDetailsDTO> getImageIdDetails(@PathVariable String imageid)
    {
        ImageDetailsDTO detailsDTO = imageDetailsService.getDetailsPerImage(imageid);
        if (detailsDTO == null)
        {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(detailsDTO);
    }
}
