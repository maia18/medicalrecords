                                                package it.unicas.rest.controller.images;

import it.unicas.persistence.entities.Images;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;

@Component
public class ImagesMapper
{

    public Images newImagesBean(ImagesDTO imagesDTO, String medicalRecordId, String filename)
    {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Images image = new Images();
        image.setDescription(imagesDTO.getDescription());
        image.setManufacturer(imagesDTO.getManufacturer());
        image.setModality(imagesDTO.getModality());
        image.setPath(medicalRecordId + "/" + filename);
        image.setSeries(imagesDTO.getSeries());
        image.setType("jpg");
        image.setRecordid(Long.parseLong(medicalRecordId));
        try
        {
            image.setStrudyDate(formatter.parse(imagesDTO.getStrudyDate()));
        }
        catch (ParseException exception)
        {
            throw new RuntimeException(exception.getMessage());
        }
        return image;
    }

    public ImagesDTO newImagesDTO(Images image)
    {
        ImagesDTO dto = new ImagesDTO();
        dto.setDescription(image.getDescription());
        dto.setManufacturer(image.getManufacturer());
        dto.setModality(image.getModality());
        dto.setPath(image.getPath());
        dto.setSeries(image.getSeries());
        dto.setType(image.getType());
        dto.setRecordId(String.valueOf(image.getRecordid()));
        dto.setStrudyDate(image.getStrudyDate().toString());
        return dto;
    }
}
