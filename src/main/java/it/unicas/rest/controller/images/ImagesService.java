package it.unicas.rest.controller.images;

import it.unicas.persistence.entities.Images;
import it.unicas.persistence.repositories.IImagesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;
import org.imgscalr.Scalr;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Component
@PropertySource("classpath:application.properties")
public class ImagesService
{

    @Autowired
    Environment env;

    private final ImagesMapper mapper;
    private final IImagesRepository repository;


    @Autowired ImagesService(ImagesMapper mapper, IImagesRepository repository)
    {
        this.mapper = mapper;
        this.repository = repository;
    }

    public String getFilesDir()
    {
        return env.getProperty("images.uploaddir")+ "/files/";
    }

    public String getThumbsDir()
    {
        return env.getProperty("images.uploaddir")+ "/thumbnail/";
    }

    @Transactional(rollbackFor = Exception.class)
    public String createImage(final ImagesDTO imagesDTO, String medicalRecordId, String filename)
    {
        Images image = mapper.newImagesBean(imagesDTO, medicalRecordId, filename);
        Images newImage = repository.save(image);
        return String.valueOf(newImage.getId());
    }

    public ImagesDTO getImageById(final String id)
    {
        Optional<Images> image = repository.findById(Long.parseLong(id));

        return image.isPresent()? mapper.newImagesDTO(image.get()) : null;
    }

    public String readAndSaveFile(MultipartFile file, String medRecId)
    {
        String filename = "";
        InputStream istream;
        try
        {
            filename = String.valueOf(new Date().getTime()) + ".jpg";
            String filesDir = getFilesDir() + medRecId + "/";
            String thumbsDir = getThumbsDir() + medRecId + "/";

            File imagesFolder = new File(filesDir);
            File thumbsFolder = new File(thumbsDir);

            imagesFolder.mkdirs();
            thumbsFolder.mkdirs();

            File outFile =
                new File(filesDir + filename);

            File outThumbnail =
                    new File(thumbsDir + filename);

            istream = file.getInputStream();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            istream.transferTo(baos);
            InputStream firstClone = new ByteArrayInputStream(baos.toByteArray());
            InputStream secondClone = new ByteArrayInputStream(baos.toByteArray());

            OutputStream ostream = new FileOutputStream(outThumbnail);
            createThumbnail(firstClone, ostream);
            FileCopyUtils.copy(secondClone, new FileOutputStream(outFile));
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
        return filename;
    }

    private void createThumbnail(InputStream stream, OutputStream ostream) throws IOException
    {
        BufferedImage image = ImageIO.read(stream);
        BufferedImage resized =
            Scalr.resize(image, Scalr.Method.SPEED, 400, Scalr.OP_ANTIALIAS, Scalr.OP_BRIGHTER);
        resized = Scalr.pad(resized, 2);

        ImageIO.write(resized, "jpg", ostream);

    }

    public List<Integer> getImagesIdsByMedRec(String medrec)
    {
        List<Images> images = repository.findByRecordid(Long.parseLong(medrec));
        List<Integer> ids = new ArrayList<>();

        for(Images image : images)
        {
            ids.add((int)image.getId());
        }
        return ids;
    }
}
