package it.unicas.rest.controller.users;

import it.unicas.rest.controller.common.ResourceAlreadyExistsException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/users")
@Secured({"ROLE_ADMIN", "ROLE_RMAN"})
public class UsersController
{
    private UsersService service;

    @Autowired
    UsersController(UsersService service)
    {
        this.service = service;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Void> create(@RequestBody UserDTO userDTO) throws URISyntaxException
    {
        ResponseEntity<Void> response = null;
        try
        {
            String userId = service.createUser(userDTO);
            response = ResponseEntity.created(new URI("api/users/" + userId)).build();
        }
        catch (ResourceAlreadyExistsException e)
        {
            response = ResponseEntity.status(HttpStatus.CONFLICT).build();
        }
        catch (UnsupportedOperationException e)
        {
            response = ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        return response;
    }

    @RequestMapping(path = "/{userId}", method = RequestMethod.PUT)
    public ResponseEntity<Void> update(@RequestBody UserDTO userDTO, @PathVariable String userId)
    {
        ResponseEntity<Void> response = null;
        try
        {
            userDTO.setUserId(userId);
            service.updateUser(userDTO);
            response = ResponseEntity.ok().build();
        }
        catch (ResourceNotFoundException e)
        {
            response = ResponseEntity.notFound().build();
        }
        catch (UnsupportedOperationException e)
        {
            response = ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        return response;
    }

    @Secured({"ROLE_ADMIN", "ROLE_RMAN", "ROLE_VIEWER"})
    @RequestMapping(path = "/{userId}", method = RequestMethod.GET)
    public ResponseEntity<UserDTO> getUserById(@PathVariable String userId)
    {
        ResponseEntity<UserDTO> response = null;
        try
        {
            UserDTO user = service.retrieveUserById(userId);
            response = ResponseEntity.ok(user);
        }
        catch (ResourceNotFoundException e)
        {
            response = ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
        return response;
    }

    @Secured({"ROLE_ADMIN", "ROLE_RMAN", "ROLE_VIEWER"})
    @GetMapping(
        params = {"userid", "document", "name", "lastname"}
    )
    public ResponseEntity<List<UserDTO>> getAllUsers(
        @RequestParam("userid") String userid,
        @RequestParam("document") String document,
        @RequestParam("name") String name,
        @RequestParam("lastname") String lastname)
    {
        List<UserDTO> dtos = service.retrieveAllUsers(userid, document, name, lastname);
        return ResponseEntity.ok(dtos);
    }

    @RequestMapping(path = "/{userId}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteUser(@PathVariable String userId)
    {
        return ResponseEntity.ok().build();
    }

}
