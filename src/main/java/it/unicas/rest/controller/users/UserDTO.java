package it.unicas.rest.controller.users;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDTO
{
    private String userId;
    private String documentType;
    private String password;
    private String documentNumber;
    private String lastName;
    private String name;
    private String gender;
    private String birthDate;
    private String birthPlace;
    private String address;
    private String phone;
    private String department;
    private String email;
    private String role;
}