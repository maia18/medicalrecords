package it.unicas.rest.controller.users;

import it.unicas.persistence.entities.DocumentType;
import it.unicas.persistence.entities.GenderType;
import it.unicas.persistence.entities.Role;
import it.unicas.persistence.entities.User;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import org.springframework.stereotype.Component;

@Component
public class UserMapper
{
    public User newUserBean(final UserDTO userDTO, Role role, final String password)
    {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        User user = new User();
        user.setId(userDTO.getUserId());
        user.setDocumentType(DocumentType.valueOf(userDTO.getDocumentType()));
        user.setEncryptedPassword(password);
        user.setDocumentNumber(userDTO.getDocumentNumber());
        user.setLastName(userDTO.getLastName());
        user.setName(userDTO.getName());
        user.setGender(GenderType.valueOf(userDTO.getGender()));
        try
        {
            user.setBirthDate(formatter.parse(userDTO.getBirthDate()));
        }
        catch (ParseException exception)
        {
            throw new RuntimeException(exception.getMessage());
        }
        user.setBirthPlace(userDTO.getBirthPlace());
        user.setAddress(userDTO.getAddress());
        user.setPhone(userDTO.getPhone());
        user.setDepartment(userDTO.getDepartment());
        user.setEmail(userDTO.getEmail());
        user.setRole(role);
        return user;
    }

    public UserDTO newUserDTO(User user)
    {
        UserDTO converted = new UserDTO();
        converted.setUserId(user.getId());
        converted.setDocumentType(user.getDocumentType().toString());
        converted.setPassword(null);
        converted.setDocumentNumber(user.getDocumentNumber());
        converted.setLastName(user.getLastName());
        converted.setName(user.getName());
        converted.setGender(user.getGender().toString());
        converted.setBirthDate(user.getBirthDate().toString());
        converted.setBirthPlace(user.getBirthPlace());
        converted.setAddress(user.getAddress());
        converted.setPhone(user.getPhone());
        converted.setDepartment(user.getDepartment());
        converted.setEmail(user.getEmail());
        converted.setRole(user.getRole().getRolename());
        return converted;
    }
}
