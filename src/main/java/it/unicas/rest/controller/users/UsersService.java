package it.unicas.rest.controller.users;

import it.unicas.persistence.entities.Role;
import it.unicas.persistence.entities.User;
import it.unicas.persistence.repositories.IRoleRepository;
import it.unicas.persistence.repositories.IUserRepository;
import it.unicas.rest.controller.common.ResourceAlreadyExistsException;
import it.unicas.security.service.ContextService;
import it.unicas.security.service.UserInformation;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class UsersService
{
    private final IUserRepository userRepository;
    private final IRoleRepository roleRepository;
    private final BCryptPasswordEncoder encoder;
    private final UserMapper userMapper;

    @Autowired
    UsersService(
        final IUserRepository userRepository,
        final IRoleRepository roleRepository,
        UserMapper mapper)
    {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.userMapper = mapper;
        this.encoder = new BCryptPasswordEncoder();
    }

    public boolean exists(final String username)
    {
        return userRepository.findById(username).isPresent();
    }

    public UserDTO retrieveUserById(final String userId)
    {
        Optional<User> foundUser = userRepository.findById(userId);
        if (foundUser.isEmpty())
        {
            throw new ResourceNotFoundException("User not found");
        }

        return userMapper.newUserDTO(foundUser.get());
    }

    public List<UserDTO> retrieveAllUsers(String userid, String document, String name, String lastname)
    {
        userid = "%" + userid + "%";
        document = "%" + document + "%";
        name = "%" + name + "%";
        lastname = "%" + lastname + "%";

        Iterable<User> users = null;
        if (ContextService.loadUserInformation().getRole().equals(Role.Roles.ROLE_ADMIN.toString()))
            users = userRepository.findAllByIdLikeAndNameLikeAndLastNameLikeAndDocumentNumberLike(userid, name, lastname, document);
        else
            users = userRepository.findByRoleNoAdmin(userid, document, name, lastname);

        List<UserDTO> dtos = new ArrayList<>();

        for (User user : users)
        {
            dtos.add(userMapper.newUserDTO(user));
        }
        return dtos;
    }

    @Transactional(rollbackFor = Exception.class)
    public String createUser(final UserDTO userDTO)
    {
        checkRoleOperation(userDTO.getRole());
        if (exists(userDTO.getUserId()))
        {
            throw new ResourceAlreadyExistsException("User already exists");
        }

        Role role = roleRepository.findByRolename(userDTO.getRole());
        User user = userMapper.newUserBean(userDTO, role, encoder.encode(userDTO.getPassword()));
        User newUser = userRepository.save(user);
        return newUser.getId();
    }

    @Transactional(rollbackFor = Exception.class)
    public String updateUser(final UserDTO userDTO)
    {
        checkRoleOperation(userDTO.getRole());
        Optional<User> user = userRepository.findById(userDTO.getUserId());

        if (user.isEmpty())
        {
            throw new ResourceNotFoundException("The user doesn't exists");
        }

        Role role = roleRepository.findByRolename(userDTO.getRole());
        String password = userDTO.getPassword();
        User newUser =
            userMapper.newUserBean(
                userDTO,
                role,
                (password == null || password.isEmpty())?
                    user.get().getEncryptedPassword() : encoder.encode(password));
        userRepository.save(newUser);
        return newUser.getId();
    }

    @Transactional(rollbackFor = Exception.class)
    public void changePassword(final String username, final String newPassword)
    {
        Optional<User> user = userRepository.findById(username);

        if (user.isEmpty())
        {
            throw new ResourceNotFoundException("The user doesn't exists");
        }
        User current = user.get();
        current.setEncryptedPassword(encoder.encode(newPassword));
        userRepository.save(current);
    }

    private void checkRoleOperation(String roleToCustomize)
    {
        UserInformation info = ContextService.loadUserInformation();
        if (Role.Roles.ROLE_RMAN.toString().equals(info.getRole()))
        {
            if (roleToCustomize.equals(Role.Roles.ROLE_RMAN.toString())
                || roleToCustomize.equals((Role.Roles.ROLE_ADMIN.toString())))
            {
                throw new UnsupportedOperationException("User can't perform this operation");
            }
        }
    }
}
