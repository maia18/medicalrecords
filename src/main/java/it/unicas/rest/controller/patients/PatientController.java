package it.unicas.rest.controller.patients;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;


import it.unicas.rest.controller.common.ResourceAlreadyExistsException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping("/api/patients")
@Secured({"ROLE_ADMIN", "ROLE_RMAN"})
public class PatientController
{
    private PatientService service;

    @Autowired
    PatientController(PatientService service)
    {
        this.service = service;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Void> create(@RequestBody PatientDTO patientDTO) throws URISyntaxException
    {
        ResponseEntity<Void> response = null;
        try {
            String patientID = service.createPatient(patientDTO);
            response = ResponseEntity.created(new URI("api/patients/" + patientID)).build();
        }
        catch (ResourceAlreadyExistsException e)
        {
            response = ResponseEntity.status(HttpStatus.CONFLICT).build();
        }
        catch (UnsupportedOperationException e)
        {
            response = ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        return response;
    }

    @RequestMapping(path = "/{patientId}", method = RequestMethod.PUT)
    public ResponseEntity<Void> update(@RequestBody PatientDTO patientDTO, @PathVariable String patientId)
    {
        ResponseEntity<Void> response = null;
        try {
            service.updatePatient(patientDTO, Long.valueOf(patientId));
            response = ResponseEntity.ok().build();
        }
        catch (ResourceNotFoundException e)
        {
            response = ResponseEntity.notFound().build();
        }
        catch (UnsupportedOperationException e)
        {
            response = ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        catch (ResourceAlreadyExistsException e)
        {
            response = ResponseEntity.status(HttpStatus.CONFLICT).build();
        }
        return response;
    }

    @RequestMapping(path = "/{patientId}", method = RequestMethod.GET)
    public ResponseEntity<PatientDTO> getPatientById(@PathVariable String patientId)
    {
        ResponseEntity<PatientDTO> response = null;
        try {
            Long patientIdlLong = Long.parseLong(patientId);
            PatientDTO patient = service.retrievePatientById(patientIdlLong);
            response = ResponseEntity.ok(patient);
        }
        catch (ResourceNotFoundException e)
        {
            response = ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
        return response;
    }

    @RequestMapping(path = "/{patientId}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deletePatient(@PathVariable String patientId)
    {
        return ResponseEntity.ok().build();
    }

    @Secured({"ROLE_ADMIN", "ROLE_RMAN", "ROLE_VIEWER", "ROLE_PROF"})
    @RequestMapping(path = "/{patientId}/details", method = RequestMethod.GET)
    public ResponseEntity<PatientDetailsDTO> getPatientDetails(@PathVariable String patientId)
    {
        ResponseEntity<PatientDetailsDTO> response = null;
        try {
            Long patientIdlLong = Long.parseLong(patientId);
            PatientDetailsDTO patientDetails = service.retrievePatientDetailsByID(patientIdlLong);
            response = ResponseEntity.ok(patientDetails);
        }
        catch (ResourceNotFoundException e)
        {
            response = ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
        return response;
    }

    @Secured({"ROLE_ADMIN", "ROLE_RMAN", "ROLE_VIEWER", "ROLE_PROF"})
    @GetMapping(
            params = {"document", "name", "lastname"}
    )
    public ResponseEntity<List<PatientDTO>> getAllUsers(
            @RequestParam("document") String document,
            @RequestParam("name") String name,
            @RequestParam("lastname") String lastname)
    {
        List<PatientDTO> dtos = service.retrieveAllPatients(document, name, lastname);
        return ResponseEntity.ok(dtos);
    }
}
