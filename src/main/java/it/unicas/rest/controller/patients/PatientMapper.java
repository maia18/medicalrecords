package it.unicas.rest.controller.patients;

import it.unicas.persistence.entities.DocumentType;
import it.unicas.persistence.entities.GenderType;
import it.unicas.persistence.entities.Patient;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;

@Component
public class PatientMapper
{
    public Patient newPatientBean(PatientDTO patientDTO)
    {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Patient patient = new Patient();
        patient.setDocId(patientDTO.getDocId());
        patient.setDocType(DocumentType.valueOf(patientDTO.getDocType()));
        patient.setLastname(patientDTO.getLastname());
        patient.setName(patientDTO.getName());
        patient.setAddress(patientDTO.getAddress());
        patient.setGender(GenderType.valueOf(patientDTO.getGender()));
        patient.setBirthplace(patientDTO.getBirthplace());
        patient.setPhone(patientDTO.getPhone());
        patient.setEmail(patientDTO.getEmail());
        patient.setStatus(patientDTO.getStatus());
        patient.setChild(patientDTO.getChild());
        //patient.setId(Long.getLong(patientDTO.getId()));
        try
        {
            patient.setBirthdate(formatter.parse(patientDTO.getBirthdate()));
        }
        catch (ParseException exception)
        {
            throw new RuntimeException(exception.getMessage());
        }
        return patient;
    }

    public PatientDTO newPatientDTO(Patient patient)
    {
        PatientDTO dto = new PatientDTO();
        dto.setDocId(patient.getDocId());
        dto.setDocType(patient.getDocType().toString());
        dto.setLastname(patient.getLastname());
        dto.setName(patient.getName());
        dto.setAddress(patient.getAddress());
        dto.setGender(patient.getGender().toString());
        dto.setBirthplace(patient.getBirthplace());
        dto.setPhone(patient.getPhone());
        dto.setEmail(patient.getEmail());
        dto.setStatus(patient.getStatus());
        dto.setChild(patient.getChild());
        dto.setId(String.valueOf(patient.getId()));
        dto.setBirthdate(patient.getBirthdate().toString());
        return dto;
    }
}
