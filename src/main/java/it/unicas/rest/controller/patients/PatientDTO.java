package it.unicas.rest.controller.patients;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class PatientDTO
{
    private String id;
    private String docType;
    private String docId;
    private String lastname;
    private String name;
    private String gender;
    private String birthdate;
    private String birthplace;
    private String address;
    private String phone;
    private String email;
    private String status;
    private String child;
}
