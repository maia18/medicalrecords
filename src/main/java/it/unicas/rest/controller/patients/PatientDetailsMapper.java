package it.unicas.rest.controller.patients;

import it.unicas.persistence.entities.MedicalRecord;
import it.unicas.persistence.entities.Patient;
import it.unicas.rest.controller.medicalrecords.MedicalRecordDTO;
import it.unicas.rest.controller.medicalrecords.MedicalRecordMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


@Component
public class PatientDetailsMapper
{
    PatientMapper patientmapper;
    MedicalRecordMapper recordMapper;

    @Autowired
    public PatientDetailsMapper(PatientMapper patientMapper, MedicalRecordMapper medicalRecordMapper)
    {
        this.patientmapper = patientMapper;
        this.recordMapper = medicalRecordMapper;
    }

    public PatientDetailsDTO newPatientDetailsDTO(Patient patient)
    {

        PatientDetailsDTO dto = new PatientDetailsDTO();
        dto.setPatient(patientmapper.newPatientDTO(patient));
        dto.setMedicalRecords(retrieveAllRecordsDTO(patient));
        return dto;
    }


    public List<MedicalRecordDTO> retrieveAllRecordsDTO(Patient patient)
    {
        List<MedicalRecord> medicalRecordList = patient.getMedicalRecordList();
        List<MedicalRecordDTO> dtos = new ArrayList<>();

        if (medicalRecordList != null)
        {
            for (MedicalRecord record : medicalRecordList)
            {
                dtos.add(recordMapper.newMedicalRecordDTO(record));
            }
        }

        return dtos;
    }
}
