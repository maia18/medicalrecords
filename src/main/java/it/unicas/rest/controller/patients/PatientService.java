package it.unicas.rest.controller.patients;


import it.unicas.persistence.entities.DocumentType;
import it.unicas.persistence.entities.MedicalRecord;
import it.unicas.persistence.entities.Patient;
import it.unicas.persistence.repositories.IPatientsRepository;
import it.unicas.rest.controller.common.ResourceAlreadyExistsException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import it.unicas.rest.controller.medicalrecords.MedicalRecordDTO;
import it.unicas.rest.controller.medicalrecords.MedicalRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


@Component
public class PatientService
{
    private final IPatientsRepository patientsRepository;
    private final PatientMapper patientMapper;
    private final PatientDetailsMapper patientDetailsMapper;
    private final MedicalRecordService medicalRecordService;


    @Autowired
    PatientService(
            final IPatientsRepository patientsRepository,
            MedicalRecordService medicalRecordService,
            PatientMapper mapper,
            PatientDetailsMapper detailsMapper

    )
    {
        this.patientsRepository = patientsRepository;
        this.patientMapper = mapper;
        this.patientDetailsMapper = detailsMapper;
        this.medicalRecordService = medicalRecordService;
    }


    public boolean exists(final PatientDTO patientDTO)
    {

        List<Patient> existingPatient =
            patientsRepository.findAllByDocIdAndDocType(
                patientDTO.getDocId(), DocumentType.valueOf(patientDTO.getDocType()));

        return !existingPatient.isEmpty();

    }

    public PatientDTO retrievePatientById(final Long patientId)
    {
        Optional<Patient> foundPatient = patientsRepository.findById(patientId);
        if (foundPatient.isEmpty())
        {
            throw new ResourceNotFoundException("Patient not Found");
        }
        return patientMapper.newPatientDTO(foundPatient.get());
    }

    public PatientDetailsDTO retrievePatientDetailsByID(final Long patientId)
    {
        Optional<Patient> foundPatient = patientsRepository.findById(patientId);
        if (foundPatient.isEmpty())
        {
            throw new ResourceNotFoundException("Patient not Found");
        }
        return patientDetailsMapper.newPatientDetailsDTO(foundPatient.get());
    }

    public List<PatientDTO> retrieveAllPatients(String document, String name, String lastname)
    {
        document = "%" + document + "%";
        name = "%" + name + "%";
        lastname = "%" + lastname + "%";

        Iterable<Patient> patients = null;
        patients = patientsRepository.findAllByDocIdLikeAndNameLikeAndLastnameLike(document, name, lastname);

        List<PatientDTO> dtos = new ArrayList<>();

        for (Patient patient : patients)
        {
            dtos.add(patientMapper.newPatientDTO(patient));
        }
        return dtos;
    }

    @Transactional(rollbackFor = Exception.class)
    public String createPatient(final PatientDTO patientDTO)
    {
        if ( exists(patientDTO))
        {
            throw new ResourceAlreadyExistsException("Patient already exists");
        }

        Patient patient = patientMapper.newPatientBean(patientDTO);
        Patient newPatient = patientsRepository.save(patient);
        String newPatientIdStr = Long.toString(newPatient.getId());
        return newPatientIdStr;
    }


    @Transactional(rollbackFor = Exception.class)
    public String updatePatient(final PatientDTO patientDTO, long patientId)
    {

        Optional<Patient> patient = patientsRepository.findById(patientId);

        if (patient.isEmpty())
        {
            throw new ResourceNotFoundException("The patient doesn't exists");
        }

        if (exists(patientDTO)) {
            Patient existingPatient =
                    patientsRepository.findByDocIdAndDocType(
                            patientDTO.getDocId(), DocumentType.valueOf(patientDTO.getDocType()));

            if( existingPatient.getId() != patientId){
                throw new ResourceAlreadyExistsException("Patient already exists");
            }

        }

        Patient newPatient =
                patientMapper.newPatientBean(patientDTO);
        newPatient.setId(patientId);
        patientsRepository.save(newPatient);
        String newPatientIdStr = Long.toString(newPatient.getId());
        return newPatientIdStr;
    }

    public PatientDTO getPatientInformationFromMedicalRecord(String medicalRecordId)
    {
        MedicalRecordDTO record = medicalRecordService.getMedicalRecord(medicalRecordId);
        PatientDTO patientDTO = this.retrievePatientById(Long.valueOf(record.getPatientId()));
        return patientDTO;
    }
}
