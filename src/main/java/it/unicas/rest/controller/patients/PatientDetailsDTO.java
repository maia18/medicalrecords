package it.unicas.rest.controller.patients;


import it.unicas.rest.controller.medicalrecords.MedicalRecordDTO;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class PatientDetailsDTO
{
    private PatientDTO patient;
    private List<MedicalRecordDTO> medicalRecords;
}
