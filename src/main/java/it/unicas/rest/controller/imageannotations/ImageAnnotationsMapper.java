package it.unicas.rest.controller.imageannotations;


import it.unicas.rest.controller.users.UserMapper;
import it.unicas.rest.controller.users.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import it.unicas.persistence.entities.ImageAnnotations;
import java.util.Date;

import java.text.ParseException;
import java.text.SimpleDateFormat;

@Component
public class ImageAnnotationsMapper
{
    UsersService usersService;
    UserMapper userMapper;

    @Autowired
    ImageAnnotationsMapper(UserMapper userMapper, UsersService usersService)
    {
        this.usersService = usersService;
        this.userMapper = userMapper;
    }

    public ImageAnnotations newImageAnnotationBean(ImageAnnotationsDTO imageAnnotationsDTO, String userID)
    {
        ImageAnnotations annotations = new ImageAnnotations();
        annotations.setAnnotationComments(imageAnnotationsDTO.getAnnotationComments());
        annotations.setImageId(imageAnnotationsDTO.getImageId());
        Date date = new Date();
        annotations.setAnnotationDate(date);
        annotations.setDoctorId(userID);
        return annotations;

    }

    public ImageAnnotationsDTO newImageAnnotationsDTO(ImageAnnotations imageAnnotations, String doctorName)
    {
        ImageAnnotationsDTO dto = new ImageAnnotationsDTO();
        dto.setImageId(imageAnnotations.getImageId());
        dto.setAnnotationComments(imageAnnotations.getAnnotationComments());
        dto.setUserId(doctorName);
        dto.setAnnotationDate(imageAnnotations.getAnnotationDate());

        return dto;
    }

}
