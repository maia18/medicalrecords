package it.unicas.rest.controller.imageannotations;

import it.unicas.rest.controller.users.UserDTO;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class ImageAnnotationsDTO {

    private String userId;
    private String imageId;
    private Date annotationDate;
    private String annotationComments;
    private UserDTO user;
}
