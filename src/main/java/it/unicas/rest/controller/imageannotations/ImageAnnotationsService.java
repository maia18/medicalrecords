package it.unicas.rest.controller.imageannotations;

import it.unicas.persistence.repositories.IImageAnnotationsRepository;
import it.unicas.rest.controller.users.UserDTO;
import it.unicas.rest.controller.users.UserMapper;
import it.unicas.rest.controller.users.UsersService;
import it.unicas.security.service.ContextService;
import it.unicas.security.service.UserInformation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import it.unicas.persistence.entities.ImageAnnotations;

import java.awt.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class ImageAnnotationsService
{
    UsersService usersService;
    UserMapper userMapper;
    ImageAnnotationsMapper imageAnnotationsMapper;
    IImageAnnotationsRepository imageAnnotationsRepository;

    @Autowired
    ImageAnnotationsService(
        UserMapper userMapper, UsersService usersService,
        ImageAnnotationsMapper imageAnnotationsMapper,
        IImageAnnotationsRepository imageAnnotationsRepository)
    {
        this.usersService = usersService;
        this.userMapper = userMapper;
        this.imageAnnotationsMapper = imageAnnotationsMapper;
        this.imageAnnotationsRepository = imageAnnotationsRepository;
    }

    @Transactional(rollbackFor = Exception.class)
    public String createAnnotation(final ImageAnnotationsDTO imageAnnotationsDTO)
    {
        UserDTO userDTO = new UserDTO();
        userDTO.setUserId(ContextService.loadUserInformation().getUsername());
        String userId = userDTO.getUserId();
        ImageAnnotations annotation = imageAnnotationsMapper.newImageAnnotationBean(imageAnnotationsDTO, userId);
        ImageAnnotations newAnnotation = imageAnnotationsRepository.save(annotation);
        return Long.toString(newAnnotation.getId());
    }

}
