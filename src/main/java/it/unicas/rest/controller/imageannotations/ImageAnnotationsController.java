package it.unicas.rest.controller.imageannotations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/imagerepo")
public class ImageAnnotationsController
{
    private final ImageAnnotationsService service;

    @Autowired
    ImageAnnotationsController(
        ImageAnnotationsService service
    )
    {
        this.service = service;
    }

    @Secured({"ROLE_ADMIN", "ROLE_PROF"})
    @RequestMapping(path = "/{imageId}", method = RequestMethod.PUT)
    public ResponseEntity<Void> create(
        @RequestBody ImageAnnotationsDTO imageAnnotationsDTO,
        @PathVariable String imageId)
    {
        ResponseEntity<Void> response = null;
        try {
            imageAnnotationsDTO.setImageId(imageId);
            service.createAnnotation(imageAnnotationsDTO);
            response = ResponseEntity.ok().build();
        }
        catch (ResourceNotFoundException e)
        {
            response = ResponseEntity.notFound().build();
        }
        catch (UnsupportedOperationException e)
        {
            response = ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        return response;
    }


}
